#ifndef BOARDSIZES
#define BOARDSIZES

const int MAX_ROW = 7;
const int MAX_COLUMN = 7;
const int MIN_ROW = 0;
const int MIN_COLUMN = 0;
const int SQUARE_COUNT = 64;

#endif // BOARDSIZES

