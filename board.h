#ifndef BOARD_H
#define BOARD_H

#include <vector>
#include <memory>

#include "pieceenums.h"

class Piece;

class Board
{
public:
    Board();

    void createPiece(int row, int column, PieceEnums::Side side, PieceEnums::Type type);
    Piece* getPieceAt(int row, int column) const;
    Piece* getPieceAt(int index) const;
    void movePiece(Piece *piece, int rowTo, int columnTo);
    int piecesCount() const;

    void clear();

private:
    void killPieceAt(int row, int column);

    std::vector<std::unique_ptr<Piece>> m_boardPieces;
};

#endif // BOARD_H
