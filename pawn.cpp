#include "pawn.h"
#include "boardsizes.h"

Pawn::Pawn(int row, int column, PieceEnums::Side side) : Piece(row, column, side)
{

}

PieceEnums::Type Pawn::type() const
{
    return PieceEnums::Pawn;
}

std::vector<Piece::Position> Pawn::getAvailableMoves(const Board &board) const
{
    std::vector<Piece::Position> moves;

    int nextRow = m_row + getDirection();
    if (nextRow >= MIN_ROW && nextRow <= MAX_ROW)
    {
        if (canHit(board, nextRow, m_column - 1))
            moves.push_back(std::make_tuple(nextRow, m_column - 1));
        if (canHit(board, nextRow, m_column + 1))
            moves.push_back(std::make_tuple(nextRow, m_column + 1));

        auto otherPiece = board.getPieceAt(nextRow, m_column);
        if (otherPiece == nullptr)
        {
            moves.push_back(std::make_tuple(nextRow, m_column));

            if (m_row == getInitialRow())
            {
                nextRow += getDirection();
                otherPiece = board.getPieceAt(nextRow, m_column);
                if (otherPiece == nullptr)
                    moves.push_back(std::make_tuple(nextRow, m_column));
            }
        }
    }

    return moves;
}

int Pawn::getDirection() const
{
    if (m_side == PieceEnums::White)
        return -1;
    if (m_side == PieceEnums::Black)
        return 1;

    return 0;
}

int Pawn::getInitialRow() const
{
    if (m_side == PieceEnums::White)
        return MAX_ROW - 1;
    if (m_side == PieceEnums::Black)
        return MIN_ROW + 1;
    return 0;
}

int Pawn::canHit(const Board& board, int row, int column) const
{
    if (column >= MIN_COLUMN && column <= MAX_COLUMN &&
        row >= MIN_ROW && row <= MAX_ROW)
    {
        auto piece = board.getPieceAt(row, column);
        if (piece != nullptr)
            if (piece->side() != side())
                return true;
    }
    return false;
}

