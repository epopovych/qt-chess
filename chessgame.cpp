#include "chessgame.h"

ChessGame::ChessGame()
    : m_currentSide(PieceEnums::None)
{

}

void ChessGame::startNewStandardGame()
{
    m_currentSide = PieceEnums::White;
    m_board.clear();

    m_board.createPiece(0, 0, PieceEnums::Black, PieceEnums::Rook);
    m_board.createPiece(0, 1, PieceEnums::Black, PieceEnums::Knight);
    m_board.createPiece(0, 2, PieceEnums::Black, PieceEnums::Bishop);
    m_board.createPiece(0, 3, PieceEnums::Black, PieceEnums::Queen);
    m_board.createPiece(0, 4, PieceEnums::Black, PieceEnums::King);
    m_board.createPiece(0, 5, PieceEnums::Black, PieceEnums::Bishop);
    m_board.createPiece(0, 6, PieceEnums::Black, PieceEnums::Knight);
    m_board.createPiece(0, 7, PieceEnums::Black, PieceEnums::Rook);
    m_board.createPiece(1, 0, PieceEnums::Black, PieceEnums::Pawn);
    m_board.createPiece(1, 1, PieceEnums::Black, PieceEnums::Pawn);
    m_board.createPiece(1, 2, PieceEnums::Black, PieceEnums::Pawn);
    m_board.createPiece(1, 3, PieceEnums::Black, PieceEnums::Pawn);
    m_board.createPiece(1, 4, PieceEnums::Black, PieceEnums::Pawn);
    m_board.createPiece(1, 5, PieceEnums::Black, PieceEnums::Pawn);
    m_board.createPiece(1, 6, PieceEnums::Black, PieceEnums::Pawn);
    m_board.createPiece(1, 7, PieceEnums::Black, PieceEnums::Pawn);
    m_board.createPiece(6, 0, PieceEnums::White, PieceEnums::Pawn);
    m_board.createPiece(6, 1, PieceEnums::White, PieceEnums::Pawn);
    m_board.createPiece(6, 2, PieceEnums::White, PieceEnums::Pawn);
    m_board.createPiece(6, 3, PieceEnums::White, PieceEnums::Pawn);
    m_board.createPiece(6, 4, PieceEnums::White, PieceEnums::Pawn);
    m_board.createPiece(6, 5, PieceEnums::White, PieceEnums::Pawn);
    m_board.createPiece(6, 6, PieceEnums::White, PieceEnums::Pawn);
    m_board.createPiece(6, 7, PieceEnums::White, PieceEnums::Pawn);
    m_board.createPiece(7, 0, PieceEnums::White, PieceEnums::Rook);
    m_board.createPiece(7, 1, PieceEnums::White, PieceEnums::Knight);
    m_board.createPiece(7, 2, PieceEnums::White, PieceEnums::Bishop);
    m_board.createPiece(7, 3, PieceEnums::White, PieceEnums::Queen);
    m_board.createPiece(7, 4, PieceEnums::White, PieceEnums::King);
    m_board.createPiece(7, 5, PieceEnums::White, PieceEnums::Bishop);
    m_board.createPiece(7, 6, PieceEnums::White, PieceEnums::Knight);
    m_board.createPiece(7, 7, PieceEnums::White, PieceEnums::Rook);
}

PieceEnums::Side ChessGame::currentSide() const
{
    return m_currentSide;
}

Piece *ChessGame::getPieceAt(int row, int column) const
{
    return m_board.getPieceAt(row, column);
}

std::vector<Piece::Position> ChessGame::getAvailableMoves(Piece *piece) const
{
    std::vector<Piece::Position> moves;

    if (piece != nullptr)
    {
        moves = piece->getAvailableMoves(m_board);
    }

    return moves;
}

Piece *ChessGame::movePiece(int rowFrom, int columnFrom, int rowTo, int columnTo)
{
    auto movedPiece = m_board.getPieceAt(rowFrom, columnFrom);

    if (movedPiece != nullptr)
    {
        m_board.movePiece(movedPiece, rowTo, columnTo);
        switchCurrentSide();
    }

    return movedPiece;
}

void ChessGame::switchCurrentSide()
{
    switch (m_currentSide)
    {
    case PieceEnums::White:
        m_currentSide = PieceEnums::Black;
        break;
    case PieceEnums::Black:
        m_currentSide = PieceEnums::White;
        break;
    }
}

