#ifndef BISHOP_H
#define BISHOP_H

#include "piece.h"

class Bishop : public Piece
{
public:    
    Bishop(int row, int column, PieceEnums::Side side);
    virtual ~Bishop() {}

    PieceEnums::Type type() const override;

    std::vector<Position> getAvailableMoves(const Board& board) const override;
};

#endif // BISHOP_H
