#ifndef PAWN_H
#define PAWN_H

#include "piece.h"

class Pawn : public Piece
{
public:
    explicit Pawn(int row, int column, PieceEnums::Side side);
    virtual ~Pawn() {}

    PieceEnums::Type type() const override;

    std::vector<Position> getAvailableMoves(const Board& board) const override;

private:
    int getDirection() const;
    int getInitialRow() const;
    int canHit(const Board &board, int row, int column) const;
};

#endif // PAWN_H
