#include "bishop.h"
#include "boardsizes.h"

Bishop::Bishop(int row, int column, PieceEnums::Side side) : Piece(row, column, side)
{

}

PieceEnums::Type Bishop::type() const
{
    return PieceEnums::Bishop;
}

std::vector<Piece::Position> Bishop::getAvailableMoves(const Board &board) const
{
    std::vector<Piece::Position> moves;

    Piece::getAvailableMovesInDirection(moves,  1,  1, board);
    Piece::getAvailableMovesInDirection(moves, -1,  1, board);
    Piece::getAvailableMovesInDirection(moves,  1, -1, board);
    Piece::getAvailableMovesInDirection(moves, -1, -1, board);

    return moves;
}
