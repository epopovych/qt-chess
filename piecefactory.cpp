#include "piecefactory.h"
#include "pawn.h"
#include "rook.h"
#include "knight.h"
#include "bishop.h"
#include "king.h"
#include "queen.h"

using namespace std;

template <typename T>
unique_ptr<Piece> createPieceImpl(int row, int column, PieceEnums::Side side)
{
    return unique_ptr<Piece> { make_unique<T>(row, column, side) };
}

unique_ptr<Piece> PieceFactory::createPiece(int row, int column, PieceEnums::Side side, PieceEnums::Type type)
{
    switch (type) {
    case PieceEnums::Pawn:
        return createPieceImpl<Pawn>(row, column, side);

    case PieceEnums::Rook:
        return createPieceImpl<Rook>(row, column, side);

    case PieceEnums::Knight:
        return createPieceImpl<Knight>(row, column, side);

    case PieceEnums::Bishop:
        return createPieceImpl<Bishop>(row, column, side);

    case PieceEnums::Queen:
        return createPieceImpl<Queen>(row, column, side);

    case PieceEnums::King:
        return createPieceImpl<King>(row, column, side);
    }

    return std::unique_ptr<Piece>();
}
