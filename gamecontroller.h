#ifndef GAMECONTROLLER_H
#define GAMECONTROLLER_H

#include <vector>
#include <memory>

#include <QObject>
#include <QQmlListProperty>
#include <QSessionManager>

#include "boardsizes.h"
#include "pieceenums.h"
#include "square.h"
#include "chessgame.h"
#include "savedgamesmanager.h"

class GameController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QQmlListProperty<Square> boardModel READ getBoardModel NOTIFY boardModelChanged)
    Q_PROPERTY(PieceEnums::Side currentSide READ currentSide NOTIFY currentSideChanged)

public:
    GameController();

    QQmlListProperty<Square> getBoardModel();
    PieceEnums::Side currentSide() const;

    Q_INVOKABLE void startNewGame();

    Q_INVOKABLE void onMouseClick(int row, int column);

signals:
    void currentSideChanged();
    void boardModelChanged();

public slots:
    void onLastWindowClosed();

private:
    void initializeSquares();
    void updateSquares();
    void updateModelData();

    bool canSelect(Square* square) const;
    void select(Square* square);
    void movePieceTo(Square* square);
    void resetSelection();

    void updateAvailableMoves();
    void resetAllAvailableMoves();

    static Square* squareAt(QQmlListProperty<Square>* property, int index);
    static int squareCount(QQmlListProperty<Square>* property);

    Square* findSquare(int row, int column);

    bool tryLoadLastGame();

    ChessGame m_game;
    SavedGamesManager m_savedGames;

    Square m_squares[SQUARE_COUNT];
    Square* m_selectedSquare;
};

#endif // GAMECONTROLLER_H
