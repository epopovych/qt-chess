#include "chessgameserializer.h"

#include <sstream>

#include <QDebug>

using namespace std;

std::string ChessGameSerializer::gameToText(const ChessGame &game)
{
    ostringstream output;

    output << game.currentSide() << endl;
    output << game.m_board.piecesCount() << endl;
    for (int i = 0; i < game.m_board.piecesCount(); ++i)
    {
        auto piece = game.m_board.getPieceAt(i);
        output << piece->type() << ' ' << piece->side() << ' ' << piece->row() << ' ' << piece->column() << endl;
    }

    return output.str();
}

bool ChessGameSerializer::textToGame(const std::string &content, ChessGame &game)
{
    istringstream input(content);

    if (!(input >> reinterpret_cast<int&>(game.m_currentSide)))
        return false;

    int count = 0;
    if (!(input >> count))
        return false;

    game.m_board.clear();

    for (int i = 0; i < count; ++i)
    {
        PieceEnums::Side side;
        PieceEnums::Type type;
        int row, column;

        if (!(input >> reinterpret_cast<int&>(type) >> reinterpret_cast<int&>(side) >> row >> column))
            return false;

        game.m_board.createPiece(row, column, side, type);
    }

    return true;
}
