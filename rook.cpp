#include "rook.h"

Rook::Rook(int row, int column, PieceEnums::Side side) : Piece(row, column, side)
{

}

PieceEnums::Type Rook::type() const
{
    return PieceEnums::Rook;
}

std::vector<Piece::Position> Rook::getAvailableMoves(const Board &board) const
{
    std::vector<Piece::Position> moves;

    Piece::getAvailableMovesInDirection(moves, -1,  0, board);
    Piece::getAvailableMovesInDirection(moves,  1,  0, board);
    Piece::getAvailableMovesInDirection(moves,  0, -1, board);
    Piece::getAvailableMovesInDirection(moves,  0,  1, board);

    return moves;
}

