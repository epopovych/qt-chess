#ifndef SAVEDGAMESMANAGER_H
#define SAVEDGAMESMANAGER_H

#include <QString>

#include "chessgame.h"

class SavedGamesManager
{
public:
    SavedGamesManager();

    QStringList getSavedGames() const;
    bool saveGame(const ChessGame& game, const QString &gameName);
    bool loadGame(ChessGame& game, const QString &gameName);

    bool lastGameExists() const;
    bool loadLastGame(ChessGame& game);
    bool saveLastGame(const ChessGame& game);

    void RemoveLastGameFileName(QStringList &entries) const;
    void RemoveExtensions(QStringList &entries) const;
    QStringList GetFilter() const;
private:
    static QString gameNameIntoFileName(const QString &gameName);
};

#endif // SAVEDGAMESMANAGER_H
