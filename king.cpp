#include "king.h"
#include "boardsizes.h"

King::King(int row, int column, PieceEnums::Side side) : Piece(row, column, side)
{

}

PieceEnums::Type King::type() const
{
    return PieceEnums::King;
}

std::vector<Piece::Position> King::getAvailableMoves(const Board &board) const
{
    std::vector<Piece::Position> moves;

    Piece::addIfLegal(moves, board, m_row - 1, m_column - 1);
    Piece::addIfLegal(moves, board, m_row - 1, m_column);
    Piece::addIfLegal(moves, board, m_row - 1, m_column + 1);
    Piece::addIfLegal(moves, board, m_row, m_column - 1);
    Piece::addIfLegal(moves, board, m_row, m_column + 1);
    Piece::addIfLegal(moves, board, m_row + 1, m_column - 1);
    Piece::addIfLegal(moves, board, m_row + 1, m_column);
    Piece::addIfLegal(moves, board, m_row + 1, m_column + 1);

    return moves;
}
