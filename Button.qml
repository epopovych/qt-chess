import QtQuick 2.0

Item {
    id: button

    property alias text: buttonText.text

    signal clicked()

    Rectangle {
        id: background
        anchors.fill: button
        color: "antiquewhite"
        border { width: 4; color: "saddlebrown"; }

        Text {
            id: buttonText
            anchors.centerIn: background
            font.pointSize: 14
            color: "saddlebrown"
        }

        MouseArea {
            id: mousearea
            anchors.fill: parent
            hoverEnabled: true

            onClicked: button.clicked()
            onEntered: buttonText.color = "orange"
            onExited: buttonText.color = "saddlebrown"
            onPressedChanged: buttonText.color = pressed ? "red" : "orange"
        }
    }
}
