#include "queen.h"

Queen::Queen(int row, int column, PieceEnums::Side side) : Piece(row, column, side)
{

}

PieceEnums::Type Queen::type() const
{
    return PieceEnums::Queen;
}

std::vector<Piece::Position> Queen::getAvailableMoves(const Board &board) const
{
    std::vector<Piece::Position> moves;

    Piece::getAvailableMovesInDirection(moves, -1, -1, board);
    Piece::getAvailableMovesInDirection(moves, -1,  0, board);
    Piece::getAvailableMovesInDirection(moves, -1,  1, board);
    Piece::getAvailableMovesInDirection(moves,  0, -1, board);
    Piece::getAvailableMovesInDirection(moves,  0,  1, board);
    Piece::getAvailableMovesInDirection(moves,  1, -1, board);
    Piece::getAvailableMovesInDirection(moves,  1,  0, board);
    Piece::getAvailableMovesInDirection(moves,  1,  1, board);

    return moves;
}

