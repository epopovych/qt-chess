import QtQuick 2.4
import QtQuick.Window 2.2
import Chess 1.0

Window {
    visible: true
    width: 800; height: 600
    MainForm {
        anchors.fill: parent

        Rectangle {
            anchors.fill: parent
            color: "gray"

            GridView {
                id: board
                anchors.centerIn: parent
                width: 400
                height: 400
                cellWidth: 50
                cellHeight: 50
                model :  gameController.boardModel
                delegate: Square {
                          id: square
                          row: model.modelData.row
                          column: model.modelData.column
                          pieceSide: model.modelData.pieceSide;
                          pieceType: model.modelData.pieceType;
                          selected: model.modelData.isSelected
                          isMoveAvailable: model.modelData.isMoveAvailable
                          color: {
                            var rowOdd = (row % 2);
                            var columnOdd = (column % 2);
                            if (rowOdd != columnOdd)
                                return "saddlebrown";
                            else
                                return "antiquewhite";
                          }

                          onClicked: {
                              gameController.onMouseClick(row, column)
                          }
                }
            }

            Text {
                id: currentTurnText
                anchors.top: board.bottom
                anchors.topMargin: 20
                anchors.horizontalCenter: board.horizontalCenter
                font.pointSize: 20
                font.bold: true

                states: [
                    State {
                        name: "WHITE"
                        when: gameController.currentSide == Piece.White
                        PropertyChanges {
                            target: currentTurnText
                            color: "white"
                            text: "Current turn: WHITE"
                        }
                    },
                    State {
                        name: "BLACK"
                        when: gameController.currentSide == Piece.Black
                        PropertyChanges {
                            target: currentTurnText
                            color: "black"
                            text: "Current turn: BLACK"
                        }
                    }
                ]
            }

            Button {
                anchors.left: board.right
                anchors.leftMargin: 10
                anchors.top: board.top
                width: 180
                height: 40
                text: "Restart"

                onClicked: gameController.startNewGame()
            }
        }
    }
}
