#ifndef CHESSGAMESERIALIZER_H
#define CHESSGAMESERIALIZER_H

#include <string>
#include <vector>

#include "chessgame.h"

class ChessGameSerializer
{
public:
    std::string gameToText(const ChessGame& game);
    bool textToGame(const std::string &content, ChessGame& game);
};

#endif // CHESSGAMESERIALIZER_H
