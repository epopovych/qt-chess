#ifndef ROOK_H
#define ROOK_H

#include "piece.h"

class Rook : public Piece
{
public:
    Rook(int row, int column, PieceEnums::Side side);
    virtual ~Rook() {}

    PieceEnums::Type type() const override;

    std::vector<Position> getAvailableMoves(const Board& board) const override;
};

#endif // ROOK_H
