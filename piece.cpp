#include "piece.h"
#include "boardsizes.h"

#include <QDebug>

Piece::Piece(int row, int column, PieceEnums::Side side) : m_side(side), m_row(row), m_column(column)
{

}

PieceEnums::Type Piece::type() const
{
    return PieceEnums::Unknown;
}

PieceEnums::Side Piece::side() const
{
    return m_side;
}

int Piece::row() const
{
    return m_row;
}

void Piece::setRow(int value)
{
    m_row = value;
}

int Piece::column() const
{
    return m_column;
}

void Piece::setColumn(int value)
{
    m_column = value;
}

void Piece::getAvailableMovesInDirection(std::vector<Piece::Position> &moves, int rowDirection, int columnDirection, const Board &board) const
{
    int nextRow = m_row + rowDirection;
    int nextColumn = m_column + columnDirection;

    while (nextRow >= MIN_ROW && nextRow <= MAX_ROW &&
           nextColumn >= MIN_COLUMN && nextColumn <= MAX_COLUMN)
    {
        auto nextPiece = board.getPieceAt(nextRow, nextColumn);

        if (nextPiece == nullptr)
        {
            moves.push_back(std::make_tuple(nextRow, nextColumn));
        }
        else
        {
            if (nextPiece->side() != side())
                moves.push_back(std::make_tuple(nextRow, nextColumn));
            break;
        }

        nextRow += rowDirection;
        nextColumn += columnDirection;
    }
}

bool Piece::isLegalMove(const Board &board, int row, int column) const
{
    if (row < MIN_ROW || row > MAX_ROW || column < MIN_COLUMN || column > MAX_COLUMN)
        return false;

    auto otherPiece = board.getPieceAt(row, column);
    if (otherPiece == nullptr)
        return true;

    if (otherPiece->side() != side())
        return true;

    return false;
}

void Piece::addIfLegal(std::vector<Piece::Position> &moves, const Board &board, int row, int column) const
{
    if (isLegalMove(board, row, column))
    {
        moves.push_back(std::make_tuple(row, column));
    }
}
