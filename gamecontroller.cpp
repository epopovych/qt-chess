#include "gamecontroller.h"
#include "piecefactory.h"
#include "chessgameserializer.h"

#include <algorithm>

#include <QDebug>

using namespace std;

GameController::GameController()
    : m_selectedSquare(nullptr)
{
    initializeSquares();

    if (!tryLoadLastGame())
        startNewGame();
}

QQmlListProperty<Square> GameController::getBoardModel()
{
    return QQmlListProperty<Square>(this, nullptr, &GameController::squareCount, &GameController::squareAt);
}

PieceEnums::Side GameController::currentSide() const
{
    return m_game.currentSide();
}

void GameController::startNewGame()
{
    m_game.startNewStandardGame();
    updateModelData();
}

void GameController::select(Square* square)
{
    if (m_selectedSquare != nullptr)
    {
        m_selectedSquare->setIsSelected(false);
    }

    m_selectedSquare = square;
    if (m_selectedSquare != nullptr)
        m_selectedSquare->setIsSelected(true);
}

void GameController::resetSelection()
{
    if (m_selectedSquare != nullptr)
    {
        m_selectedSquare->setIsSelected(false);
        m_selectedSquare = nullptr;
    }
}

void GameController::movePieceTo(Square *newSquare)
{
    if (newSquare == nullptr || m_selectedSquare == nullptr)
        return;

    auto piece = m_game.movePiece(m_selectedSquare->row(), m_selectedSquare->column(), newSquare->row(), newSquare->column());
    if (piece == nullptr)
        return;

    m_selectedSquare->setPiece(nullptr);
    newSquare->setPiece(piece);

    resetSelection();

    emit currentSideChanged();
}

void GameController::onMouseClick(int row, int column)
{
    auto clickedSquare = findSquare(row, column);

    if (clickedSquare->isMoveAvailable())
    {
        movePieceTo(clickedSquare);
    }
    else if (canSelect(clickedSquare))
    {
        select(clickedSquare);
    }
    else
    {
        resetSelection();
    }

    updateAvailableMoves();
}

void GameController::onLastWindowClosed()
{
    m_savedGames.saveLastGame(m_game);
}

void GameController::initializeSquares()
{
    for (int row = 0; row <= MAX_ROW; ++row)
        for (int column = 0; column <= MAX_COLUMN; ++column)
        {
            int index = row * 8 + column;
            m_squares[index].setRow(row);
            m_squares[index].setColumn(column);
        }
}

void GameController::updateSquares()
{
    for_each(begin(m_squares), end(m_squares), [&](Square& square)
    {
        auto piece = m_game.getPieceAt(square.row(), square.column());
        square.setPiece(piece);
    });
}

bool GameController::canSelect(Square *square) const
{
    if (square == nullptr)
        return false;

    if (square->isSelected())
        return false;

    if (square->piece() == nullptr)
        return false;

    if (square->pieceSide() != m_game.currentSide())
        return false;

    return true;
}

void GameController::updateAvailableMoves()
{
    resetAllAvailableMoves();

    if (m_selectedSquare == nullptr)
        return;
    if (m_selectedSquare->piece() == nullptr)
        return;

    auto availableMoves = m_game.getAvailableMoves(m_selectedSquare->piece());

    for_each(begin(availableMoves), end(availableMoves), [&](const Piece::Position& position)
    {
       int row = get<0>(position);
       int column = get<1>(position);

       auto square = findSquare(row, column);

       if (square != nullptr)
       {
           square->setIsMoveAvailable(true);
       }
    });
}

void GameController::resetAllAvailableMoves()
{
    for_each(begin(m_squares), end(m_squares), [](Square& square)
    {
       square.setIsMoveAvailable(false);
    });
}

Square* GameController::squareAt(QQmlListProperty<Square>* property, int index)
{
    auto controller = qobject_cast<GameController*>(property->object);
    if (controller != nullptr)
        return &controller->m_squares[index];

    return nullptr;
}

int GameController::squareCount(QQmlListProperty<Square> *)
{
    return SQUARE_COUNT;
}

Square *GameController::findSquare(int row, int column)
{
    auto result = find_if(begin(m_squares), end(m_squares), [=](Square& square)
    {
       return (square.row() == row) && (square.column() == column);
    });

    Square* square = result != end(m_squares)
            ? &(*result)
            : nullptr;

    return square;
}

void GameController::updateModelData()
{
    updateSquares();
    emit currentSideChanged();
}

bool GameController::tryLoadLastGame()
{
    if (m_savedGames.lastGameExists())
    {
        if (m_savedGames.loadLastGame(m_game))
        {
            updateModelData();
            return true;
        }
    }

    return false;
}

