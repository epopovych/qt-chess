#include "savedgamesmanager.h"
#include "chessgameserializer.h"

#include <QFile>
#include <QDir>
#include <QTextStream>

#include <algorithm>

const QString LastGameName = "__lastgame";
const QString Ext = ".txt";
const QString Path = "Saves\\";

SavedGamesManager::SavedGamesManager()
{
    if (!QDir::current().exists(Path))
        QDir::current().mkdir(Path);
    getSavedGames();
}

void SavedGamesManager::RemoveLastGameFileName(QStringList& entries) const
{
    entries.removeOne(LastGameName + Ext);
}

void SavedGamesManager::RemoveExtensions(QStringList& entries) const
{
    std::for_each(std::begin(entries), std::end(entries), [](QString& entry)
    {
       int index = entry.lastIndexOf(Ext);
       if (index != -1)
       {
           entry.remove(index, Ext.length());
       }
    });
}

QStringList SavedGamesManager::GetFilter() const
{
    QStringList filter;
    filter << "*.txt";
    return filter;
}

QStringList SavedGamesManager::getSavedGames() const
{
    QDir dir(Path);    
    auto entries = dir.entryList(GetFilter());

    RemoveLastGameFileName(entries);
    RemoveExtensions(entries);

    return entries;
}

bool SavedGamesManager::saveGame(const ChessGame &game, const QString &gameName)
{
    ChessGameSerializer serializer;
    auto content = serializer.gameToText(game);

    auto filename = gameNameIntoFileName(gameName);

    QFile file(filename);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text))
    {
        return false;
    }

    QTextStream stream(&file);
    stream << QString::fromStdString(content);
    stream.flush();

    file.close();

    return true;
}

bool SavedGamesManager::loadGame(ChessGame &game, const QString &gameName)
{
    auto filename = gameNameIntoFileName(gameName);

    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        return false;
    }

    QTextStream stream(&file);
    auto content = stream.readAll().toStdString();
    file.close();

    ChessGameSerializer serializer;
    return serializer.textToGame(content, game);
}

bool SavedGamesManager::lastGameExists() const
{
    return QFile::exists(gameNameIntoFileName(LastGameName));
}

bool SavedGamesManager::loadLastGame(ChessGame &game)
{
    return loadGame(game, LastGameName);
}

bool SavedGamesManager::saveLastGame(const ChessGame &game)
{
    return saveGame(game, LastGameName);
}

QString SavedGamesManager::gameNameIntoFileName(const QString &gameName)
{
    return Path + gameName + Ext;
}

