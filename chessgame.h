#ifndef CHESSGAME_H
#define CHESSGAME_H

#include "piece.h"

class ChessGame
{
    friend class ChessGameSerializer;

public:
    ChessGame();

    void startNewStandardGame();

    PieceEnums::Side currentSide() const;

    Piece* getPieceAt(int row, int column) const;
    std::vector<Piece::Position> getAvailableMoves(Piece* piece) const;
    Piece* movePiece(int rowFrom, int columnFrom, int rowTo, int columnTo);

protected:
    void switchCurrentSide();

    Board m_board;
    PieceEnums::Side m_currentSide;
};

#endif // CHESSGAME_H
