#include "square.h"
#include "piecefactory.h"

Square::Square(QObject *parent)
    : QObject(parent)
    , m_piece(nullptr)
    , m_row(-1)
    , m_column(-1)
    , m_isSelected(false)
    , m_isMoveAvailable(false)
{

}

Square::Square(int row, int column, Piece *piece)
    : QObject()
    , m_row(row)
    , m_column(column)
    , m_piece(piece)
    , m_isSelected(false)
    , m_isMoveAvailable(false)
{
}

int Square::row() const
{
    return m_row;
}

void Square::setRow(int value)
{
    m_row = value;
}

int Square::column() const
{
    return m_column;
}

void Square::setColumn(int value)
{
    m_column = value;
}

bool Square::isSelected() const
{
    return m_isSelected;
}

void Square::setIsSelected(bool value)
{
    if (m_isSelected != value)
    {
        m_isSelected = value;
        emit isSelectedChanged();
    }
}

bool Square::isMoveAvailable() const
{
    return m_isMoveAvailable;
}

void Square::setIsMoveAvailable(bool value)
{
    if (m_isMoveAvailable != value)
    {
        m_isMoveAvailable = value;
        emit isMoveAvailableChanged();
    }
}

PieceEnums::Type Square::pieceType() const
{
    if (m_piece != nullptr)
        return m_piece->type();
    return PieceEnums::Unknown;
}

PieceEnums::Side Square::pieceSide() const
{
    if (m_piece != nullptr)
        return m_piece->side();
    return PieceEnums::None;
}

Piece *Square::piece() const
{
    return m_piece;
}

void Square::setPiece(Piece *value)
{
    if (m_piece != value)
    {
        m_piece = value;
        emit pieceChanged();
    }
}
