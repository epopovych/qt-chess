TEMPLATE = app

QT += qml quick

SOURCES += main.cpp \
    piece.cpp \
    gamecontroller.cpp \
    square.cpp \
    pawn.cpp \
    rook.cpp \
    knight.cpp \
    bishop.cpp \
    queen.cpp \
    king.cpp \
    piecefactory.cpp \
    board.cpp \
    chessgame.cpp \
    chessgameserializer.cpp \
    savedgamesmanager.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    piece.h \
    gamecontroller.h \
    square.h \
    pawn.h \
    rook.h \
    knight.h \
    bishop.h \
    queen.h \
    king.h \
    piecefactory.h \
    boardsizes.h \
    pieceenums.h \
    board.h \
    chessgame.h \
    chessgameserializer.h \
    savedgamesmanager.h
