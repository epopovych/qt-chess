#ifndef KING_H
#define KING_H

#include "piece.h"

class King : public Piece
{
public:
    King(int row, int column, PieceEnums::Side side);
    virtual ~King() {}

    PieceEnums::Type type() const override;

    std::vector<Position> getAvailableMoves(const Board& board) const override;
};

#endif // KING_H
