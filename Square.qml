import QtQuick 2.0
import Chess 1.0

Item {
    id: container
    width: 50
    height: 50

    property int row
    property int column
    property alias color : rect.color
    property int pieceType : Piece.Unknown
    property int pieceSide : Piece.None
    property bool selected : false
    property bool isMoveAvailable : false

    signal clicked(int row, int column)

    Rectangle {
        id: rect
        anchors.fill: container
    }

    Image {
        anchors.centerIn: container
        source: getPieceImageSource()
    }

    Rectangle {
        id: selectionHighlight
        anchors.fill: container
        color: "transparent"
        border { width: 2; color: "red" }
        visible: false

        Rectangle {
            id: selectionHighlightBody
            color: "red"
            opacity: 0.4
            anchors.fill: selectionHighlight
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: container.clicked(row, column)
    }

    states: [
        State {
            name: "SELECTED"
            when: selected
            PropertyChanges {
                target: selectionHighlight
                visible: true
                border.color: "red"
                border.width: 2
            }
            PropertyChanges {
                target: selectionHighlightBody
                color: "red"
            }
        },

        State {
            name: "MOVE_AVAILABLE"
            when: isMoveAvailable
            PropertyChanges {
                target: selectionHighlight
                visible: true
                border.color: "blue"
                border.width: 1
            }
            PropertyChanges {
                target: selectionHighlightBody
                color: "blue"
            }
        }
    ]

    function getPieceImageSource()
    {
        var basePath = "images/pieces/";

        var sidePath = "";
        switch (pieceSide)
        {
        case Piece.White:
            sidePath = "white/";
            break;
        case Piece.Black:
            sidePath = "black/";
            break;
        default:
            return "";
        }

        var pieceName = "";
        switch(pieceType)
        {
        case Piece.Pawn:
            pieceName = "pawn";
            break;
        case Piece.Rook:
            pieceName = "rook";
            break;
        case Piece.Knight:
            pieceName = "knight";
            break;
        case Piece.Bishop:
            pieceName = "bishop";
            break;
        case Piece.Queen:
            pieceName = "queen";
            break;
        case Piece.King:
            pieceName = "king";
            break;
        default:
            return "";
        }

        var ext = ".svg";

        return basePath + sidePath + pieceName + ext
    }
}
