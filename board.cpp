#include "board.h"
#include "piecefactory.h"

using namespace std;

Board::Board()
{

}

void Board::createPiece(int row, int column, PieceEnums::Side side, PieceEnums::Type type)
{
    auto piece = PieceFactory::createPiece(row, column, side, type);
    if (piece != nullptr)
        m_boardPieces.push_back(std::move(piece));
}

Piece *Board::getPieceAt(int row, int column) const
{
    auto result = find_if(begin(m_boardPieces), end(m_boardPieces), [=](const unique_ptr<Piece>& piece) -> bool {
       return (piece->row() == row && piece->column() == column);
    });

    Piece* piece = result != end(m_boardPieces)
       ? (*result).get()
       : nullptr;

    return piece;
}

Piece *Board::getPieceAt(int index) const
{
    if (index < 0 || index >= m_boardPieces.size())
        return nullptr;

    return m_boardPieces[index].get();
}

void Board::movePiece(Piece* movedPiece, int rowTo, int columnTo)
{
    killPieceAt(rowTo, columnTo);

    movedPiece->setRow(rowTo);
    movedPiece->setColumn(columnTo);
}

int Board::piecesCount() const
{
    return static_cast<int>(m_boardPieces.size());
}

void Board::clear()
{
    m_boardPieces.clear();
}

void Board::killPieceAt(int row, int column)
{
    auto killedPiece = getPieceAt(row, column);

    if (killedPiece != nullptr)
    {
        m_boardPieces.erase(remove_if(begin(m_boardPieces), end(m_boardPieces), [=](const unique_ptr<Piece>& piece) {
           return piece.get() == killedPiece;
        }));
    }
}

