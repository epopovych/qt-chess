#ifndef PIECEFACTORY_H
#define PIECEFACTORY_H

#include <memory>

#include "piece.h"

class PieceFactory
{
public:
    static std::unique_ptr<Piece> createPiece(int row, int column, PieceEnums::Side side, PieceEnums::Type type);
};

#endif // PIECEFACTORY_H
