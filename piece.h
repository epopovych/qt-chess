#ifndef PIECE_H
#define PIECE_H

#include <vector>
#include <tuple>

#include "pieceenums.h"
#include "board.h"

class Piece {
public:
    typedef std::tuple<int, int> Position;

    Piece(int row, int column, PieceEnums::Side side);
    virtual ~Piece() {}

    virtual PieceEnums::Type type() const;
    PieceEnums::Side side() const;

    int row() const;
    void setRow(int value);
    int column() const;
    void setColumn(int value);

    virtual std::vector<Position> getAvailableMoves(const Board& board) const = 0;

protected:
    void getAvailableMovesInDirection(std::vector<Position>& moves, int rowDirection, int columnDirection, const Board& board) const;
    bool isLegalMove(const Board& board, int row, int column) const;
    void addIfLegal(std::vector<Position>& moves, const Board& board, int row, int column) const;

    PieceEnums::Side m_side;
    int m_row;
    int m_column;
};

#endif // PIECE_H
