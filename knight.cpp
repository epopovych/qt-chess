#include "knight.h"

Knight::Knight(int row, int column, PieceEnums::Side side) : Piece(row, column, side)
{

}

PieceEnums::Type Knight::type() const
{
    return PieceEnums::Knight;
}

std::vector<Piece::Position> Knight::getAvailableMoves(const Board &board) const
{
    std::vector<Piece::Position> moves;

    Piece::addIfLegal(moves, board, m_row - 2, m_column - 1);
    Piece::addIfLegal(moves, board, m_row - 2, m_column + 1);
    Piece::addIfLegal(moves, board, m_row - 1, m_column - 2);
    Piece::addIfLegal(moves, board, m_row - 1, m_column + 2);
    Piece::addIfLegal(moves, board, m_row + 1, m_column - 2);
    Piece::addIfLegal(moves, board, m_row + 1, m_column + 2);
    Piece::addIfLegal(moves, board, m_row + 2, m_column - 1);
    Piece::addIfLegal(moves, board, m_row + 2, m_column + 1);

    return moves;
}

