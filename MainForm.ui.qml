import QtQuick 2.4

Rectangle {
    property alias mouseArea: mouseArea

    width: 800
    height: 600

    MouseArea {
        id: mouseArea
        width: 800
        height: 600
        anchors.fill: parent
    }
}
