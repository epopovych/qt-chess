#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQml>

#include "piece.h"
#include "square.h"
#include "gamecontroller.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    qmlRegisterUncreatableType<PieceEnums>("Chess", 1, 0, "Piece", "");
    qmlRegisterType<Square>("Chess", 1, 0, "SquareModel");

    QQmlApplicationEngine engine;

    GameController gameController;
    auto context = engine.rootContext();
    context->setContextProperty("gameController", &gameController);

    QObject::connect(&app, &QGuiApplication::lastWindowClosed,
                     &gameController, &GameController::onLastWindowClosed);

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
