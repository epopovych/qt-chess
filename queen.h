#ifndef QUEEN_H
#define QUEEN_H

#include "piece.h"

class Queen : public Piece
{
public:
    Queen(int row, int column, PieceEnums::Side side);
    virtual ~Queen() {}

    PieceEnums::Type type() const override;

    std::vector<Position> getAvailableMoves(const Board& board) const override;
};

#endif // QUEEN_H
