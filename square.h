#ifndef SQUARE_H
#define SQUARE_H

#include <memory>
#include <QObject>

#include "piece.h"

class Square : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int row READ row)
    Q_PROPERTY(int column READ column)
    Q_PROPERTY(PieceEnums::Type pieceType READ pieceType NOTIFY pieceChanged)
    Q_PROPERTY(PieceEnums::Side pieceSide READ pieceSide NOTIFY pieceChanged)
    Q_PROPERTY(bool isSelected READ isSelected WRITE setIsSelected NOTIFY isSelectedChanged)
    Q_PROPERTY(bool isMoveAvailable READ isMoveAvailable WRITE setIsMoveAvailable NOTIFY isMoveAvailableChanged)

public:
    explicit Square(QObject *parent = 0);
    Square(int row, int column, Piece* piece = nullptr);

    int row() const;
    void setRow(int value);
    int column() const;
    void setColumn(int value);

    bool isSelected() const;
    void setIsSelected(bool value);

    bool isMoveAvailable() const;
    void setIsMoveAvailable(bool value);

    PieceEnums::Type pieceType() const;
    PieceEnums::Side pieceSide() const;
    Piece* piece() const;
    void setPiece(Piece* value);

signals:
    void isSelectedChanged();
    void isMoveAvailableChanged();
    void pieceChanged();

public slots:

private:
    int m_row;
    int m_column;
    Piece* m_piece;
    bool m_isSelected;
    bool m_isMoveAvailable;
};

#endif // SQUARE_H
