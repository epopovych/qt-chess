#ifndef PIECEENUMS
#define PIECEENUMS

#include <QObject>

class PieceEnums : public QObject
{
    Q_OBJECT
    Q_ENUMS(Type)
    Q_ENUMS(Side)

public:
    enum Type
    {
        Unknown,
        Pawn,
        Rook,
        Knight,
        Bishop,
        Queen,
        King
    };

    enum Side
    {
        None,
        White,
        Black
    };

};

#endif // PIECEENUMS

